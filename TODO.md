* config file location should have a sensible default
* config file location should be able to be specified as an argument
* when no holds have been set, shutdown after last application is finished
* provide option to log to a file
* provide option to log each app to a separate file
* make a function to print help for -h/--help
