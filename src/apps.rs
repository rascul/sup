use std::boxed::Box;
use std::fs::File;
use std::io::Read;
use std::path::PathBuf;

use colored::Colorize;
use log::error;
use mio::{Poll, PollOpt, Ready};
use serde_derive::Deserialize;

use crate::app::App;
use crate::proc::Proc;
use crate::result::Result;

/// List of apps to start and monitor
#[derive(Debug, Deserialize)]
pub struct Apps {
	/// List of apps to start and monitor
	#[serde(rename = "app")]
	apps: Vec<App>,
}

impl Apps {
	/// Load apps from toml file
	pub fn load(p: PathBuf) -> Result<Self> {
		let path: String = p.display().to_string();

		match File::open(p) {
			Ok(mut file) => {
				let mut buf = String::new();

				if let Err(e) = file.read_to_string(&mut buf) {
					error!("unable to read apps file: {}", path);
					return Err(Box::new(e));
				};

				match toml::from_str(&buf) {
					Ok(t) => Ok(t),
					Err(e) => {
						error!("Invalid toml in apps file: {}", path);
						Err(Box::new(e))
					}
				}
			}
			Err(e) => {
				error!("Unable to open apps file: {}", path);
				Err(Box::new(e))
			}
		}
	}

	/// Get a list of the apps as a `Vec<App>`
	pub fn apps(&self) -> Vec<App> {
		self.apps.clone()
	}

	/// start all the apps
	pub fn start(&self) -> (Vec<Proc>, Option<i8>, Poll) {
		// the managed processes
		let mut procs: Vec<Proc> = Vec::new();

		// number of things to hold on
		let mut holds: Option<i8> = None;

		// this should be changed to not unwrap
		let poll = Poll::new().unwrap();

		for app in self.apps() {
			// app needs to be waited on before starting the next one
			if app.wait {
				if let Err(e) = app.wait_start() {
					error!("[{}] failed to start: {:?}", &app.name.yellow(), e);
				}
			}
			// app can go in the background
			else {
				let name = app.name.clone();

				match Proc::start(app) {
					Ok(proc) => {
						// see if we need to hold on this app
						if proc.app.hold {
							holds = Some(holds.unwrap_or(0) + 1);
						}

						// register the app to poll the status
						poll.register(
							&proc.process,
							proc.token,
							Ready::readable(),
							PollOpt::edge(),
						)
						.unwrap(); // gotta get rid of this unwrap

						procs.push(proc);
					}
					Err(e) => {
						error!("[{}] failed to start: {:?}", name.yellow(), e);
					}
				};
			}
		}

		(procs, holds, poll)
	}
}
