use std::convert::TryInto;
use std::process::{Command, Stdio};

use colored::Colorize;
use log::info;

use mio::Token;
use mio_child_process::{CommandAsync, Process};

use crate::app::App;
use crate::result::Result;

pub struct Proc {
	pub app: App,
	pub process: Process,
	pub token: Token,
}

impl Proc {
	pub fn start(app: App) -> Result<Self> {
		info!("[{}] starting", &app.name.yellow());

		let mut command = Command::new(&app.command);
		command.stdout(Stdio::piped());
		command.stderr(Stdio::piped());

		command.args(&app.args);

		let process = command.spawn_async()?;
		let token = Token(process.id().try_into()?);

		Ok(Proc {
			app,
			process,
			token,
		})
	}

	pub fn stop(&mut self) {
		info!("[{}] stopping", &self.app.name.yellow());
		let _ = self.process.kill();
	}
}
