use std::process::Command;

use colored::Colorize;
use log::{error, info};
use serde_derive::Deserialize;

use crate::result::Result;

/// structure to define an app config
#[derive(Clone, Debug, Deserialize)]
pub struct App {
	/// name of the app
	pub name: String,

	/// path/command to run
	pub command: String,

	/// arguments
	#[serde(default = "Vec::new")]
	pub args: Vec<String>,

	/// should the app be restarted if it exits successfully
	#[serde(default = "ret_false")]
	pub restart_on_success: bool,

	/// should the app be restarted if it exits unsuccessfully
	#[serde(default = "ret_false")]
	pub restart_on_failure: bool,

	/// should the app be restarted if it is killed (SIGKILL)
	#[serde(default = "ret_false")]
	pub restart_on_terminate: bool,

	/// on sup startup, should we wait for this app to run before continuting
	#[serde(default = "ret_false")]
	pub wait: bool,

	/// should this app keep sup from exiting, if it's running
	#[serde(default = "ret_false")]
	pub hold: bool,
}

fn ret_false() -> bool {
	false
}

impl App {
	/// start the application and wait for it to exit
	pub fn wait_start(&self) -> Result<()> {
		info!("[{}] starting", &self.name.yellow());

		let mut command = Command::new(&self.command);
		command.args(&self.args);

		match command.output() {
			Ok(output) => {
				if !output.stdout.is_empty() {
					let s = String::from_utf8(output.stdout)?;
					for line in s.lines() {
						info!("[{}] stdout: {}", &self.name.yellow(), line);
					}
				}
				if !output.stderr.is_empty() {
					let s = String::from_utf8(output.stderr)?;
					for line in s.lines() {
						error!("[{}] stderr: {}", &self.name.yellow(), line);
					}
				}
				Ok(())
			}
			Err(e) => Err(Box::from(e)),
		}
	}
}
