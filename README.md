[![Pipeline Status](https://gitlab.com/rascul/sup/badges/master/pipeline.svg)](https://gitlab.com/rascul/sup/pipelines)
[![MIT License](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/rascul/sup/blob/master/LICENSE)

Sup is a small process supervisor. It is originally to monitor some
applications in my X session but it can be used for other things.

## Installing

```shell
$ git clone https://gitlab.com/rascul/sup
$ cd sup
$ cargo install --path .
```

## Running

Sup must find the config file apps.toml in the config directory. By default
this is `~/.config/sup` but can be set with `-c /path/to/directory`. Also
inside this directory is where the log file will be stored.

## Features

### Restart

Sup can be configured to restart an application when it stops. There are three
conditions sup can watch for:

* Success: In this case, the application has done a normal shutdown and the
exit status is 0.
* Failure: If the application exits with a non zero exit status it is
considered to have failed.
* Terminated: The application was killed by some other mechanism, such as with
SIGKILL.

### Holds

A hold may be placed on any number of application. If there are any holds, sup
will keep track and while any held applications are running, sup will not exit.
When all held applications are no longer running, and are not configured to
restart anymore, sup will close the other applications and shutdown. If no holds
are specified, sup will start apps until it realizes there are no holds then
proceed to exit.

## Configuration File

The configuration file is sup.toml and will be found in the current directory
when starting sup. This will change in the future. Sup uses the
[toml](https://github.com/toml-lang/toml) format to describe each application
to supervise.

### `[[app]]`

Each application instance to monitor will have an `[[app]]` section. The apps
will be started in the order as read from the file.

#### Required Entries

* name: The name of the application.
* command: The command to run, including path if necessary.
* args: An array of arguments to pass with the command. May be an empty array.

#### Optional Entries

* restart_on_success: Restart the app if it exits successfully. (Default: false)
* restart_on_failure: Restart the app if it fails. (Default: false)
* restart_on_terminate: Restart the app if it is terminated. (Default: false)
* wait: Don't run the next app until this one is finished. (Default: false)
* hold: Don't exit sup until this app has finished. (Default: false)

### Example

```toml
[[app]]
name = "x resource"
command = "xrdb"
args = ["~/.Xresources"]
wait = true

[[app]]
name = "terminal"
command = "alacritty"
restart_on_success = false
restart_on_failure = true
restart_on_terminate = true
hold = true

[[app]]
name = "environment"
command = "alacritty"
args = ["-e", "env"]

[[app]]
name = "window manager"
command = "twm"
hold = true
```

## Dependencies

### Older Dependencies

A couple deps (mio and signal-hook) are getting a bit old. Mio changed the API
with v0.7.0 and I'm currently unable to put forth the time required to update
it.

# Tree

Output of `cargo tree`:

~~~
sup v0.1.0 (/home/rascul/src/sup)
├── chrono v0.4.19
│   ├── libc v0.2.126
│   ├── num-integer v0.1.45
│   │   └── num-traits v0.2.15
│   │       [build-dependencies]
│   │       └── autocfg v1.1.0
│   │   [build-dependencies]
│   │   └── autocfg v1.1.0
│   ├── num-traits v0.2.15 (*)
│   └── time v0.1.44
│       └── libc v0.2.126
├── colored v2.0.0
│   ├── atty v0.2.14
│   │   └── libc v0.2.126
│   └── lazy_static v1.4.0
├── dirs v4.0.0
│   └── dirs-sys v0.3.7
│       └── libc v0.2.126
├── fern v0.6.1
│   ├── colored v1.9.3
│   │   ├── atty v0.2.14 (*)
│   │   └── lazy_static v1.4.0
│   └── log v0.4.17
│       └── cfg-if v1.0.0
├── getopts v0.2.21
│   └── unicode-width v0.1.9
├── log v0.4.17 (*)
├── mio v0.6.23
│   ├── cfg-if v0.1.10
│   ├── iovec v0.1.4
│   │   └── libc v0.2.126
│   ├── libc v0.2.126
│   ├── log v0.4.17 (*)
│   ├── net2 v0.2.37
│   │   ├── cfg-if v0.1.10
│   │   └── libc v0.2.126
│   └── slab v0.4.7
│       [build-dependencies]
│       └── autocfg v1.1.0
├── mio-child-process v0.2.1
│   ├── libc v0.2.126
│   ├── mio v0.6.23 (*)
│   └── mio-extras v2.0.6
│       ├── lazycell v1.3.0
│       ├── log v0.4.17 (*)
│       ├── mio v0.6.23 (*)
│       └── slab v0.4.7 (*)
├── serde v1.0.140
├── serde_derive v1.0.140 (proc-macro)
│   ├── proc-macro2 v1.0.40
│   │   └── unicode-ident v1.0.2
│   ├── quote v1.0.20
│   │   └── proc-macro2 v1.0.40 (*)
│   └── syn v1.0.98
│       ├── proc-macro2 v1.0.40 (*)
│       ├── quote v1.0.20 (*)
│       └── unicode-ident v1.0.2
├── signal-hook v0.1.17
│   ├── libc v0.2.126
│   ├── mio v0.6.23 (*)
│   └── signal-hook-registry v1.4.0
│       └── libc v0.2.126
└── toml v0.5.9
    └── serde v1.0.140
~~~
